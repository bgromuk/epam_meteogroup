package com.meteogroup.meteo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.meteogroup.meteo.data.ObservationItem;
import com.meteogroup.meteo.data.ObservationItemRep;
import org.apache.avro.Schema;
import org.apache.avro.data.Json;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.reflect.AvroSchema;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.*;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
@Configuration
@EnableBatchProcessing
@EnableJpaRepositories
@EnableScheduling
public class MeteoApplication {


	@Autowired
	EntityManagerFactory entityManagerFactory;

	@Autowired
	ObservationItemRep observationItemRep;

	public static void main(String[] args) {
		SpringApplication.run(MeteoApplication.class, args);
	}

	@Bean
	public Job importJob(JobBuilderFactory jobBuilderFactory, Step importObservationStep) {
		return jobBuilderFactory.get("importJob")
				.incrementer(new RunIdIncrementer())
				.start(importObservationStep)
				.build();
	}

	@Bean
	public Step importObservationStep(StepBuilderFactory stepBuilderFactory, ItemStreamReader reader, ItemWriter writer) {
		return stepBuilderFactory.get("importObservationStep")
				.chunk(2)
				.reader(reader)
				.writer(writer)
				.build();
	}

	@Bean(destroyMethod = "")
	@JobScope
	public ItemStreamReader<ObservationItem> reader() {
		JpaPagingItemReader reader = new JpaPagingItemReader();
		reader.setEntityManagerFactory(entityManagerFactory);
		reader.setQueryString("SELECT s FROM ObservationItem s");
		reader.setPageSize(100);
		return reader;
	}

	@Bean(destroyMethod = "")
	@JobScope
	public ItemWriter<ObservationItem> writer() {
		AvroWriter avroWriter = new AvroWriter();
		return avroWriter;
	}

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	@Scheduled(cron = "0 * * * * *")
	public void run() throws Exception{
		jobLauncher.run(job, new JobParameters());
	}

}
