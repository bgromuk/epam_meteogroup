package com.meteogroup.meteo;

import com.meteogroup.meteo.data.ObservationItem;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bgro on 6/22/2017.
 */
public class AvroWriter implements ItemWriter<ObservationItem> {

    private static final String FILENAME = "filename.txt";

    private DatumWriter<Observation> obsDatumWriter= new SpecificDatumWriter<Observation>(
                Observation.class);

    @Override
    public void write(List<? extends ObservationItem> list) throws Exception {
        Observation obs = null;
        List<Observation> itemsToSave = new ArrayList<>();
        for (ObservationItem observation : list) {
            obs = Observation.newBuilder()
            .setStationId(observation.getStationId())
            .setHumiduty(observation.getHumiduty())
            .setLatitude(observation.getLatitude())
            .setLongtitude(observation.getLongtitude())
            .setPreasure(observation.getPreasure())
            .setTemperature(observation.getTemperature())
            .setWindSpeed(observation.getWindSpeed()).build();

            itemsToSave.add(obs);
        }

        try (DataFileWriter<Observation> obsFileWriter = new DataFileWriter<Observation>(obsDatumWriter))
        {
            obsFileWriter.create(obs.getSchema() ,new File(FILENAME));
            for (Observation observation : itemsToSave) {
                obsFileWriter.append(observation);
            }
            obsFileWriter.flush();
        }
    }
}
