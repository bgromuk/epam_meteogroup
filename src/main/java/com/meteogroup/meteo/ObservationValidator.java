package com.meteogroup.meteo;

import com.meteogroup.meteo.Observation;
import com.meteogroup.meteo.data.ObservationItem;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ObservationValidator implements Validator {


    public static final double LOW_BND_LAT = -90.0;
    public static final double HIGH_BND_LAT = 90.0;

    public static final double LOW_BND_LONG = -180.0;
    public static final double HIGH_BND_LONG = 180.0;

    public static final int MIN_HUM_PERC = 0;
    public static final int MAX_HUM_PERC = 100;

    public boolean supports(Class clazz) {
        return ObservationItem.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        ObservationItem observation = (ObservationItem) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stationId", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "latitude", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longtitude", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "windSpeed", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "preasure", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "humiduty", "field.required");


        if (observation.getStationId() == null) {
            if (observation.getLatitude() != null){
                if (observation.getLatitude() < LOW_BND_LAT) {
                    errors.rejectValue("latitude", "field.min.length",
                            new Object[]{Double.valueOf(LOW_BND_LAT)},
                            "The latitude min value must be at least " + LOW_BND_LAT + " .");
                }


                if (observation.getLatitude() > HIGH_BND_LAT) {
                    errors.rejectValue("latitude", "field.max.length",
                            new Object[]{Double.valueOf(HIGH_BND_LAT)},
                            "The latitude max value must be not more than " + HIGH_BND_LAT + " .");
                }
            }

            if (observation.getLongtitude() != null){
                if (observation.getLongtitude() < LOW_BND_LONG) {
                    errors.rejectValue("longtitude", "field.min.length",
                            new Object[]{Double.valueOf(LOW_BND_LONG)},
                            "The longtitude min value must be at least " + LOW_BND_LONG + " .");
                }


                if (observation.getLongtitude() > HIGH_BND_LONG) {
                    errors.rejectValue("longtitude", "field.max.length",
                            new Object[]{Double.valueOf(HIGH_BND_LONG)},
                            "The longtitude max value must be not more than " + HIGH_BND_LAT + " .");
                }
            }

            if (observation.getHumiduty() != null){
                if (observation.getHumiduty() < MIN_HUM_PERC) {
                    errors.rejectValue("humiduty", "field.min.length",
                            new Object[]{Integer.valueOf(MIN_HUM_PERC)},
                            "The humiduty min value must be at least " + MIN_HUM_PERC + " .");
                }


                if (observation.getHumiduty() > MAX_HUM_PERC) {
                    errors.rejectValue("humiduty", "field.max.length",
                            new Object[]{Integer.valueOf(MAX_HUM_PERC)},
                            "The humiduty max value must be not more than " + MAX_HUM_PERC + " .");
                }
            }
        }
    }
}
