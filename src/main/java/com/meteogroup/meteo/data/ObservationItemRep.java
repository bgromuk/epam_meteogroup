package com.meteogroup.meteo.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rabbit on 23.06.17.
 */
@Transactional
public interface ObservationItemRep extends JpaRepository<ObservationItem, String> {

}
