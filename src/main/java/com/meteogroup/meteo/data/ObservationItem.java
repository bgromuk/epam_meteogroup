package com.meteogroup.meteo.data;


import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

/**
 * Created by rabbit on 20.06.17.
 */
@Entity
@Table(name = "observations")
public class ObservationItem {
    @Id
    @GeneratedValue
    private Integer id;

    private Integer stationId;

    private Double latitude;
    private Double longtitude;
    // Knots
    private Integer windSpeed;
    //Celcius degreeses
    private Integer temperature;
    // Milibars
    private Integer preasure;
    // Percents
    private Integer humiduty;

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

    public Integer getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Integer windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getPreasure() {
        return preasure;
    }

    public void setPreasure(Integer preasure) {
        this.preasure = preasure;
    }

    public Integer getHumiduty() {
        return humiduty;
    }

    public void setHumiduty(Integer humiduty) {
        this.humiduty = humiduty;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
