package com.meteogroup.meteo;

import com.meteogroup.meteo.Observation;
import com.meteogroup.meteo.data.ObservationItem;
import com.meteogroup.meteo.data.ObservationItemRep;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rabbit on 21.06.17.
 */
@RestController
@RequestMapping(value="/observation")
public class MainController {
    @Autowired
    ObservationItemRep observationItemRep;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new ObservationValidator());
    }

    @RequestMapping(value="", method = RequestMethod.POST)
    public ResponseEntity<ObservationItem> post(ObservationItem observation) {
        observationItemRep.save(observation);
        //writer.write(observation);
        return new ResponseEntity<ObservationItem>(observation, HttpStatus.OK);

    }

    @RequestMapping(value="/test")
    public void test() {
        ObservationItem item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(25d);
        item.setLongtitude(13d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(7);
        item.setHumiduty(60);
        item.setLatitude(12d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(7);
        item.setHumiduty(14);
        item.setLatitude(25d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);


        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);


        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);


        observationItemRep.save(item);

        item = new ObservationItem();
        item.setStationId(2);
        item.setHumiduty(50);
        item.setLatitude(50d);
        item.setLongtitude(50d);

        observationItemRep.save(item);


    }

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @RequestMapping("/launch")
    public void handle() throws Exception{
        jobLauncher.run(job, new JobParameters());
    }


}
